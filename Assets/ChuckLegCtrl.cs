﻿using UnityEngine;
using System.Collections;

public class ChuckLegCtrl : MonoBehaviour {
    bool upward, rotating;

	// Use this for initialization
	void Start () {
        upward = true;
        rotating = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (rotating)
        {
            if (transform.rotation.eulerAngles.z >= 45 && transform.rotation.eulerAngles.z <= 100)
                upward = false;
            else if (transform.rotation.eulerAngles.z <= 315 && transform.rotation.eulerAngles.z >= 100)
                upward = true;
            if (upward)
                transform.Rotate(0, 0, 50 * Time.deltaTime);
            else
                transform.Rotate(0, 0, -50 * Time.deltaTime);
        }
	}
    public void startRotation()
    {
        rotating = true;
    }
    public float stopRotation()
    {
        rotating = false;
        audio.Play();
        return transform.rotation.eulerAngles.z + 40;
    }
    public bool isRotating()
    {
        return rotating;
    }
}
