﻿using UnityEngine;
using System.Collections;

public class HudGui : MonoBehaviour
{
    public PlayaCtrl player;
    public gameCtrl game;

    public Texture primaryColor;
    public Texture secondaryColor;
    public Texture shell;
    public Texture[] Heads;

    void OnGUI()
    {
        if (!game)
            return;

        Vector2 pos;

        //Power launch
        if (game.powerIntensity >= 1f)
        {
            pos = new Vector2((Screen.width/2)-102, Screen.height - 28);
            GUI.DrawTexture(new Rect(pos.x, pos.y, 204, 24), secondaryColor);
            GUI.DrawTexture(new Rect(pos.x + 2, pos.y + 2, game.powerIntensity * 2, 20), primaryColor);
        }

        // Ammo
        pos = new Vector2(8, 8);
        for (int i = 0; i < game.ammo; i++)
        {
            GUI.DrawTexture(new Rect(pos.x, pos.y, 40, 80), shell);
            pos.x += 40;
        }

        //Heads
        pos = new Vector2(Screen.width-88, 8);
        for (int i = 0; i < game.subLives; i++)
        {
            if (i >= Heads.Length)
                break;
            GUI.DrawTexture(new Rect(pos.x, pos.y, 60, 60), Heads[i]);
            pos.x -= 88;
        }
    }
}