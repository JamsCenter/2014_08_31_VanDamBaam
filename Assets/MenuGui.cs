﻿using UnityEngine;
using System.Collections;

public class MenuGui : MonoBehaviour {
    bool startScreen = true;
    public Texture startTexture;
    bool credits = false;
    public Texture creditTexture;
            
    void OnGUI () {
        if (startScreen)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), startTexture);
            if (Input.anyKey)
            {
                startScreen = false;
                credits = true;
            }
        }
        else if (credits)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), creditTexture);
            Invoke("StopCredits", 5);
        }
        else
        {
            // Make a background box
            var pos = new Vector2(Screen.width / 2, Screen.height / 2);
            pos.x -= 125;
            pos.y -= 125;

            GUI.Box(new Rect(pos.x, pos.y, 250, 80), "Van Dam Baam");

            if (GUI.Button(new Rect(pos.x + 30, pos.y + 40, 80, 20), "Play"))
            {
                Application.LoadLevel(1);
            }
            if (GUI.Button(new Rect(pos.x + 140, pos.y + 40, 80, 20), "Run away"))
            {
                Debug.Log("Todo : find a way to run away from JCVD");
                Application.Quit();
            }
        }
    }
    void StopCredits()
    {
        credits = false;
    }
}