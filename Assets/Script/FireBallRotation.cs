﻿using UnityEngine;
using System.Collections;

public class FireBallRotation : MonoBehaviour {
	
	public float rotation=90;
	public float rotateAround=90;
	public Transform center;

	void Update () {
		this.transform.Rotate (Vector3.forward* rotation*Time.deltaTime);
		if (center != null)
			this.transform.RotateAround (center.position,Vector3.forward,rotateAround*Time.deltaTime);
	}
}
