﻿using UnityEngine;
using System.Collections;

public class GraphicalVanDamme : MonoBehaviour {
	public GameObject Prefab;
	public GameObject SceneObjectToFollow;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.A)) {
			MakeOne();
		}
		var tmp = transform.position;
		tmp.x = SceneObjectToFollow.transform.position.x;
		tmp.y = SceneObjectToFollow.transform.position.y;
		transform.position = tmp;
		//if (go){go.transform.Translate (new Vector2 (10, 10));}
	}
	GameObject go;
	void MakeOne(){
		go = (GameObject)Instantiate (Prefab);
		GameObject torso = go.transform.FindChild ("p_torso").gameObject;
		torso.GetComponent<HingeJoint2D>().connectedBody = this.rigidbody2D;
		torso.rigidbody2D.AddForce (new Vector2(10, 10)); // random force to get it moving

		var tmp = go.transform.position;
		tmp.y = tmp.y + 10;
		tmp.x = tmp.x + 10;
		go.transform.position = tmp;
		go.transform.Translate (new Vector2 (10, 10));
	}
}
